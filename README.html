<title> palo PA-RISC/Linux Boot Loader </title>
<body bgcolor=white text=black vlink=navy alink=red link=blue>
<h1 align=center> <i>palo</i> PA-RISC/Linux Boot Loader </h1>

<h2 align=center>Building <i>palo</i></h2>
<center><table border=1 bgcolor=yellow width=80%><tr><td>
If you are self hosted on PA-RISC/Linux you should already have
<i>palo</i> on your system.  Assuming you have <i>apt</i> configured
you can always run "<tt>apt update; apt install palo</tt>" to
obtain the latest released version.
<p>
If you need to build <i>palo</i> yourself, especially if you are
cross-compiling on x86, skip to the end of this document.
</table></center>

<h2 align=center>Kernel Command Line</h2>
<i>Palo</i>'s boot loader
parses the kernel command line to determine which kernel
to boot and optionally which ramdisk to load.
Given a command line: '0/vmlinux&nbsp;...&nbsp;initrd=0/ramdisk&nbsp;...',
palo uses '0/vmlinux' as the kernel file and '0/ramdisk' as
the ramdisk file.
Both names are composed of a partition number followed by a file
path name within that partition.
Partition number '0' is magic and refers to
the kernel (ramdisk) you placed on the boot medium with '-k' ('-r').
The path name on the '0' partition is ignored, '0/vmlinux' and '0/xyzzy'
work identically, but it's a good idea to be consistent with what
you'll call kernels and ramdisks on an ext2/3/4 partition.

<h2 align=center>Creating and Maintaining Bootable Disks </h2>
Partitioned media usually refers to disks, in this case disks partitioned
by <i>fdisk</i> or a similar program.
Normally your disk is properly initialized when you install
PA-RISC Linux, but if you need to do it by hand here are some clues:

<ul>
    <li>Use <i>fdisk</i> or something to (re)partition the disk.  When
    you are finished, the disk may need to have a partition of type
    "F0" somewhere in the first 2Gb.  This is where the boot loader
    and an optional kernel and ramdisk will be stored, so make it big
    enough for that -- at least 4Mb (I like 8-16Mb).  There must also
    be an ext2/3/4 partition within the first 2Gb where you'll store your
    Linux kernel.

    <li>If <i>fdisk</i> created the first partition at sector 2048,
    which is usual for modern fdisk, you don't need a F0 partition, so
    set all partition types to 82 swap or 83 linux and palo will place
    the boot loader inside the disk label.  You still need a /boot
    partition <2GB from the beginning of the disk formatted as
    ext2/3/4 where you keep your boot kernels and initial ramdisks.

    <p><li>Use <i>mke2fs</i> and <i>mkswap</i> as usual.

    <p><li>Mount the partition, copy your kernel and any other
    interesting files to it, unmount.

    <p><li>Use <i>palo</i> to make the disk bootable:
    <pre>
	$ palo \
	     -c "5/vmlinux HOME=/" \
	     -k path/to/vmlinux \
	     -I /dev/your-disk</pre>
<center><table border=1 bgcolor=yellow width=80%><tr><td>
On a self-hosted system, you can accomplish the same thing by placing
the following into <tt>/etc/palo.conf</tt>:
<pre>--commandline=5/vmlinux HOME=/
--recoverykernel=/boot/vmlinux
--init-partitioned=/dev/your-disk</pre>
and running <i>palo</i>.
</table></center>

    <p> <b>-k&nbsp;path/to/vmlinux</b> designates a kernel to be stored
    along with the boot loader in the "F0" partition.
    You can omit this if you want, since you'll usually be booting
    a kernel from an ext2/3/4 partition instead.  I like to have a kernel
    there just in case the ext2/3/4 one is lost for some reason.  I
    call it a <i><b>recovery kernel</b></i>.

    <p> <b>-c&nbsp;5/vmlinux</b> must be changed for your situation.  The
    number, 5, is the partition number of your ext2/3/4 partition as
    reported by fdisk, the same number you use
    when you mounted the partition (e.g., "mount /dev/sdb5 /mnt").
    "/vmlinux" is the path to
    your kernel executable within your ext2/3/4 partition.
    The special partition number "0" is used to load the "recovery"
    kernel which you placed into the "F0" partition with <b>-k</b>.

    <p><b>-I /dev/your-disk</b> tells <i>palo</i> to initialize the <i>palo</i> boot
    parameters on the drive rather than preserving any existing
    parameters.

    <p>Use '<tt>palo -?</tt>' or '<tt>palo --help</tt>' for more information.
</ul>

<p>You now have a partitioned bootable disk, try it!

<p>There is no need to run <i>palo</i> when you change the
kernel in your ext2/3/4 partition because it is located dynamically
by the <i>palo</i> boot loader when you boot.  However there are probably
some tasks which will again require <i>palo</i>.

<p>Once you have a partitioned bootable disk, <i>palo</i>
can be used to change the default command line or kernel or
boot loader.  The most common task is probably changing the "recovery"
kernel:
<center><table border=5 bgcolor=red><tr><th><font color=white>The update (-U)
feature is currently disabled, perhaps permanently.  The usual method
for maintaining your disk is to edit /etc/palo.conf and rerun <i>palo</i>.
</font></table></center>
    <pre>
	./palo -k path/to/vmlinux -U /dev/your-disk</pre>
or command line:
    <pre>
	./palo -c "new command line" -U /dev/your-disk</pre>
Which will normally be done in a self-hosted environment by
editing /etc/palo.conf and re-running palo.
Hopefully you won't need to use <i>palo</i> very often.
<h2 align=center>Creating ISO9660 Bootable CD-ROMs</h2>
Bootable CDs are often used for installation.
In short the process is:
<ol>
<li>Master your CD image but don't burn it.
<b>The image MUST contain the kernel(s), iplboot, and ramdisk file (if used).</b>
<p><li>Run <i>palo</i> to make the image bootable:
    <pre>
	./palo  -k path/to/vmlinux \
		-b iplboot \
		-c '0/vmlinux ....' \
		-C your-iso-image</pre>
    <p><b>-C</b> tells <i>palo</i> to prepare a CD-ROM image.  'iplboot'
    and 'path/to/vmlinux' <b>must be exactly the same files</b> (same
    contents) you previously
    copied into the future root file system or <i>palo</i> will fail.
    I usually point those paths at the exact files in the directory
    from which I mastered the CD just to be sure.
<p><li>Burn the CD and boot it.
</ol>
<p>CD-ROM support at the moment is a bit of a hack.  Here's how it
works.
<p><i>palo</i> currently treats CD-ROM as an unpartitioned sequential
medium like tape or bootp.  Unlike other unpartitioned media, there
is no room to store the kernel and bootloader (iplboot) near the
start of the (ISO-standard) medium, so <i>palo</i> requires you
to put those files
into the ISO file system.  HP boot firmware requires the boot loader
(iplboot) to be a multiple of 2k in length and be
stored contiguously on a 2k boundary.  Luckily the ISO file system
meets all these criteria except for the mod-2k length, which is achieved
by padding iplboot.
The palo bootloader (iplboot) requires the kernel to be stored
contiguously (except when booting from ext2/3/4), and the ISO file system
works well for this too.

<p>When you run <i>palo</i>, it locates the boot loader and kernel
(optionally ramdisk)
files in the ISO file system by doing a raw search through the ISO
image.  That's why the files in the ISO file system, and those named
on the <i>palo</i> command line must be identical.
Once found, pointers to those files are stored in the appropriate
places in the boot headers.

<p>Ideally <i>palo</i> and the boot loader would both understand
the ISO file system, but that'll take more investment than I'm
interested in supplying.  If anyone pursues this, note that I've
had good luck leveraging code from <i>aboot</i>, one of the Alpha
boot loaders.  I recommend starting with the ISO code from
aboot-0.6 or later.

<p>It may be possible to place a MS-DOS partition table on a CD-ROM
therefore having an "F0" partition as on a hard disk, but I don't feel
confident this would be understandable by, say, a Windows box.

<h2 align=center>Some Newer Features</h2>
During installation testing several annoyances were discovered
which have been partly addressed by two new features:
<ol>
<li>HP machines can use either a serial or a graphics boot console.
<i>Palo</i> now figures out which one you are using and adds
the appropriate "console=" to the end of the boot command line.
It is only added if you don't already have "console=" in your
boot command line.

<p><li>It would be nice to have a single bootable image, especially
when using CD-ROM.  This requires booting a 32-bit or 64-bit kernel as
appropriate for the hardware.
<i>Palo</i> can now place both a 32 and 64-bit kernel on sequential
(or CD-ROM) media.  Simply use the <b>-k</b> (or <b>--recoverykernel</b>)
option twice, once for each kernel.
The <i>palo</i> boot loader examines the kernel name, which
on sequential&CD-ROM is often "0/vmlinux", and if it ends in "32" or "64" 
<i>palo</i> boots the requested kernel.  However if the name does not
end in "32" or "64", <i>palo</i> chooses a kernel based upon a
recommendation by firmware, which is almost always the right thing.
(Note that a 32-bit kernel will be chosen to a 64-bit one on hardware
which can boot both.  Change the kernel name to 0/vmlinux64 if you
want to force a 64-bit kernel.)
</ol>

<h2 align=center> Original Goals</h2>
<ul>
    <li> #1 support target is PA/Linux on a disk by itself
    	 (another disk(s) may contain a standard HP-UX installation)
    <li> #2 support target is PA/Linux with kernel on sequential media such
	 as tape or tftp/bootp (or CD-ROM)
    <li> #3 support target is PA/Linux kernel on HP-UX disk with
	 second disk for PA/Linux file system.
	 (NOTE this is currently impossible with the ELF32 compiler
	 <font color=red>**This is now a non-goal**</font>
	 )
    <li> Not a support target: dual-boot HP-UX/Linux systems with
	 both Linux and HP-UX file systems on a single disk
    <li> Possibility: write mnttab into palo so you can boot with
	"normal" path names on that machine.  If palo knew where
	to find /etc/mnttab...
</ul>
<h2 align=center> Features </h2>
PA/Linux partitioned hard disks:
<ul>
    <li> can be read/written by current Linux boxes, expecially x86
    <li> can be read/written by IA-64 Linux boxes
    <li> are tolerated by IA-32 and IA-64 non-Linux boxes
    <li> are not understood by the HP-UX secondary boot loader
    <li> require no new disk partitioning tools
</ul>
<h2 align=center> Terminology </h2>
<i>palo</i> is two programs, a <b>boot loader</b>, which is loaded by the
HP firmware into memory and then executed, and <b>boot media management
tool</b>, which initializes and updates bootable media such as disks.
The <i>palo</i> boot loader is stored in a file called <b>iplboot</b>.
"IPL" is HP jargon for Initial Program Loader.  It's mostly called
"the palo boot loader" in this document.
<p> The boot media management tool is called <b>palo</b>, just as
on x86 the LILO boot media management tool is called <b>lilo</b>,
though it's worth noting that <b>palo</b> doesn't usually need to be
used every time you build a new kernel, as <b>lilo</b> does.
<h2 align=center> Basic Media Format </h2>
Bootable PA/Linux disks
combine a standard MS-DOS partition table
with the data required for HP firmware to locate its boot loader,
all within the first 512-byte "sector" of the disk.
Here is the detailed layout of the first 512 bytes of the disk.
<b><i>Only these bytes can be depended upon!</i></b>
The term "IPL" means Initial Program Loader e.g., boot loader in HP-ese.
<pre>
	Offset (hex)		Contents	Why

	0			0x80		These two bytes denote
	1			0x00		a LIF volume to HP firmware

	f0-f3			IPL_ADDR	disk offset to beginning
						of boot "IPL" loader.  Must
						not be zero.  Must be a
						multiple of 2kbytes.
						Big endian.

	f4-f7			IPL_SIZE	Size of boot loader in bytes.
						Must be a multiple of 2kbytes.
						Big endian.

	f8-f11			IPL_ENTRY	Offset from the beginning
						of the boot loader to its
						entry point.  This really
						better be a multiple of 4bytes.
						Big endian.

	1b0-1ff			P-TABLE		DOS partition table, managed
						by fdisk.
</pre>

Information about the kernel, command line, and ramdisk, is
sandwiched between the LIF magic number and the IPL_ADDR.  Check out
<b>struct firstblock</b> in 
<tt>common.h</tt> for the details.

The boot program must be located
<b><i>within the first 2 Gb of the boot medium</i></b>
(a limitation of older machines, which might not be permanent).

<p><i>palo</i> works with both un-partitioned (usually sequential) media
such as tapes, and partitioned (usually random-access) media such
as disks.  The media format for un-partitioned media is described first
since it is a subset of the format for partitioned media.
In the remaining discussion, the term sequential is synonymous with
un-partitioned, and random-access is synonymous with partitioned
media.  This restriction is not present in the <i>palo</i> software however.

<p>On <b>unpartitioned</b> media,
the partition table shown in the previous figure is unused and
set to values which will not be mistaken for a partition table.
The boot loader program is stored starting
at 2kbytes from the beginning of the medium and is followed
by the kernel file and optional ramdisk file.

<p>On <b>partitioned</b> media, which is usually random-access, for example
disks, the boot loader program must be stored in an area protected
from disk management software, and often cannot be located at the
first 2kbyte boundary as on sequential media.
<i>palo</i> therefore places
the boot loader, and optionally a kernel and ramdisk,
in a special partition,
created by <i>fdisk</i>, of type <b>F0</b>.
HP-UX firmware requires the boot program to be stored on the boot
medium starting on a multiple
of 2kbytes, whereas the <b>F0</b> partition might start on a 512-byte
sector boundary.  The boot program starts within the <b>F0</b> partition
on the first 2k boundary, which may be up to 3 sectors from the start
of the partition.

<p>The format of the boot loader, kernel, and ramdisk are identical
to the sequential case except that some padding is added in order
that somewhat larger kernels and boot loaders can be added later
without re-writing the rest of the F0 partition (this feature may
not yet be supported by <i>palo</i> however).

<p>On partitioned media, <i>palo</i> can load a kernel from any
ext2/3/4-formatted partition which falls within the first 2G of the
medium, in addition to having a "sequential" kernel, perhaps best seen
as a recovery kernel, within the <b>F0</b> partition.


<h2 align=center>Really Building <i>palo</i></h2>
In the source directory type 'make'.  On x86 you will need the
PA-RISC cross compilers installed and in your $PATH.
<p>
You can use "<tt>make DESTDIR=/ install</tt>" to install <i>palo</i>
in the normal location.  On x86 the cross compilers are usually
in "<tt>/opt/palinux</tt>" so I use
"<tt>make DESTDIR=/opt/palinux install</tt>".
Note that the <i>palo</i> executable goes into "<tt>DESTDIR/<b>sbin</b></tt>"
which you may want to add to your $PATH.
<p>
<i>Palo</i> can also be built in the normal way as a Debian package
though it cannot be cross-compiled as a Debian package.
