/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */

struct loadsegment
{
    long int offset;			/* byte offset within executable file */
    unsigned   length;			/* length of segment in file */
    unsigned   zeros;			/* # zero bytes at end of segment */
    unsigned mem;			/* target memory address for segment */
};

struct loadable
{
    unsigned offset0;			/* offset of "first" segment in file */
    unsigned entry;			/* entry point address */
    unsigned first;			/* lowest address */
    unsigned size;			/* size, not including BSS */
    unsigned uncompressed_size;		/* if compressed: real file size */
    char *uncompressed_data;		/* real (uncompressed) file data */
    int n;				/* # valid segs below */
    struct loadsegment segment[10];	/* up to 10 segments */
};

#define PREPARE_CONTINUE	0	/* not the right kind of kernel */
#define PREPARE_FATAL		1	/* right kind, but some fatal error */
#define PREPARE_OK		2	/* all's well */

/* load.c */
int load_loadable(char *target, int fd, struct loadable *loadable);
int prepare_loadable(int fd, struct loadable *loadable, int *wide);

int prepare_ELF32_loadable(int fd, struct loadable *loadable, int *wide);
int prepare_ELF64_loadable(int fd, struct loadable *loadable, int *wide);
int prepare_GZIP_loadable(int fd, struct loadable *loadable, int *wide);

