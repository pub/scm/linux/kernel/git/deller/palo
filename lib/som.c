/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */
#include "common.h"
#include "load.h"
#include <linux/som.h>
#include <stdio.h>

int prepare_SOM_loadable(int fd, struct loadable *loadable, int *wide)
{
    struct som_hdr sh;
    struct som_exec_auxhdr aux;

    *wide = 0;

    STRUCTREAD(fd, sh, 0);
    /* check for a good SOM header */
    if (__be16_to_cpu(sh.system_id) != SOM_SID_PARISC_1_1 ||
	__be16_to_cpu(sh.a_magic) != SOM_EXEC_NONSHARE)
    {
	return PREPARE_CONTINUE;
    }

    printf("SOM-format executable %08x\n", sh.aux_header_location);

    STRUCTREAD(fd, aux, __be32_to_cpu(sh.aux_header_location));

    loadable->segment[0].offset = __be32_to_cpu(aux.exec_tfile);
    loadable->offset0 = loadable->segment[0].offset;
    loadable->segment[0].length = __be32_to_cpu(aux.exec_tsize);
    loadable->segment[0].mem = __be32_to_cpu(aux.exec_tmem);
    loadable->segment[0].zeros = __be32_to_cpu(aux.exec_dmem)
	- (__be32_to_cpu(aux.exec_tmem) + __be32_to_cpu(aux.exec_tsize));
    loadable->segment[1].offset = __be32_to_cpu(aux.exec_dfile);
    loadable->segment[1].length = __be32_to_cpu(aux.exec_dsize);
    loadable->segment[1].mem = __be32_to_cpu(aux.exec_dmem);
    loadable->segment[1].zeros = __be32_to_cpu(aux.exec_bsize);

    loadable->entry = __be32_to_cpu(aux.exec_entry);
    loadable->first = __be32_to_cpu(aux.exec_tmem);
    loadable->size = __be32_to_cpu(aux.exec_dmem) +
		__be32_to_cpu(aux.exec_dsize) - __be32_to_cpu(aux.exec_tmem);
    loadable->n = 2;

    return PREPARE_OK;
}
