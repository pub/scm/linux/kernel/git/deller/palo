#ifndef __PART_H
#define __PART_H
/* stuff formerly from <linux/genhd.h> */
#define PALO_PARTITION                  0xf0
#define LINUX_EXT2_PARTITION            0x83
#define LINUX_SWAP_PARTITION            0x82
#define LINUX_RAID_PARTITION            0xfd    /* autodetect RAID partition */
#define DOS_EXTENDED_PARTITION 5
#define WIN98_EXTENDED_PARTITION 0x0f
#define LINUX_EXTENDED_PARTITION 0x85


struct partition {
        unsigned char boot_ind;         /* 0x80 - active */
        unsigned char head;             /* starting head */
        unsigned char sector;           /* starting sector */
        unsigned char cyl;              /* starting cylinder */
        unsigned char sys_ind;          /* What partition type */
        unsigned char end_head;         /* end head */
        unsigned char end_sector;       /* end sector */
        unsigned char end_cyl;          /* end cylinder */
        unsigned int start_sect;        /* starting sector counting from 0 */
        unsigned int nr_sects;          /* nr of sectors in partition */
} __attribute__((packed));

/******/
#endif /* __PART_H */
