/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */
#include <sys/types.h>
#include "common.h"

extern void error(int number, ...);

/* paloio.c */
extern int seekwrite(int fd, char *buf, unsigned size, __u64 where);
extern int seekread(int fd, char *buf, unsigned size, __u64 where);
extern int cat(int out, int in);
extern int fsize(int fd);
