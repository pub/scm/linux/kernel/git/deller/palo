/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

static char *errormessages[] = {
	/* 0 */
#include "usage.h"
	,

	/* 1 */
	"The output file you requested does not exist.  If you are trying\n"
	"to create a file appropriate for booting from sequential media\n"
	"such as a tape or bootp, please add the -s option.\n",

	/* 2 */
	"You must provide both a kernel and a bootloader when creating a\n"
	"sequentially-bootable file.  There is no support for modifying an\n"
	"existing sequentially-bootable file.\n",

	/* 3 */
	"Maximum command line size is %d bytes, yours is %d bytes\n",

	/* 4 */
	"The boot loader magic number is 0x%x, I need it to be 0x%x\n",

	/* 5 */
	"Boot loader text+data is %d bytes, maximum allowed is 256k\n",

	/* 6 */
	"Problem with %s while checking boot loader on media\n",

	/* 7 */
	"Your current media does contain not the magic values required by\n"
	"PARISC booting firmware or perhaps it has some values and they\n"
	"are inconsistent with the F0 partition on your disk.  If you\n"
	"want this media to be a PARISC/Linux bootable device, potentially\n"
	"losing its existing (for example, Intel x86) bootability, please\n"
	"re-run this command command adding the -I command-line option.\n",

	/* 8 */
	"The firmware boot header on your media refers to a boot loader\n"
	"which has an invalid checksum, so is corrupt in some way.  You'll\n"
	"need to re-run this command adding the -I option to place a good\n"
	"boot loader on your media.\n",

	/* 9 */
	"Your boot loader executable will not fit within the F0 partition\n"
	"available on your media.\n",

	/* 10 */
	"Your media does not contain a partition table.  Use 'fdisk' or\n"
	"a similar utility to create one.  Be certain to create a partition\n"
	"of type F0 to hold the boot loader, possibly a kernel image, and\n"
	"possibly a ramdisk image.  5-6Mbytes is probably a good size\n",

	/* 11 */
	"Your media does not contain a partition type F0.  Use 'fdisk' or\n"
	"a similar utility to create one.  It should be large enough to\n"
	"hold the boot loader, possibly a kernel image, and\n"
	"possibly a ramdisk image.  5-6Mbytes is probably a good size\n",

	/* 12 */
	"your '-b file' should be the output of mkbootable, not ELF or SOM\n",

	/* 13 */
	"A fatal error occurred while loading your boot loader into RAM\n",

	/* 14 */
	"Your %s is too big for your F0 partition\n",

	/* 15 */
	"Unable to locate %s on ISO image\n",

	/* 16 */
	"%s is not a recognizable executable (ELF32 or ELF64)\n",

	/* 17 */
	"Too many of the same type of kernel.  %s is either the second\n"
	"32-bit kernel or the second 64-bit one.\n",

	/* 18 */
	"For ext2/3-formatted palo partitions, you cannot specify a kernel\n"
	"or a ramdisk.\n",

	/* 19 */
	"mke2fs failed, exit code %d.",

	/* 20 */
	"Cannot update an uninitialized ext2/3 formatted partition.\n"

};

#define NMESSAGES (sizeof errormessages / sizeof errormessages[0])

void
error(int number, ...)
{
    va_list args;

    if (number < 0 || number >= NMESSAGES)
    {
	fprintf(stdout, "Unknown error number %d\n", number);
    }
    else
    {
	va_start(args, number);
	vfprintf(stdout, errormessages[number], args);
	va_end(args);
    }
    exit(2);
}
