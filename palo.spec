Name:		palo
Version:	2.27
Release:	1%{?dist}
Summary:	PALO - PA-RISC Boot Loader
Packager:	Helge Deller <deller@gmx.de>
Group:		System Environment/Base
License:	GPL
URL:		http://git.kernel.org/cgit/linux/kernel/git/deller/palo.git/
Source:		%{name}.tgz
BuildRequires:	gcc lynx gzip binutils-hppa-linux-gnu gcc-hppa-linux-gnu
# Requires:	

%description
PALO - Linux boot loader and boot media management tool for PA-RISC/HPPA.

%global debug_package %{nil}

%prep
%setup -c

%build
#make iplboot # missing libc-cross headers for that
make

%install
make install DESTDIR=%{buildroot} IFLAGS=""
# move palo binary from /sbin to /usr/bin
mkdir -p %{buildroot}/%{_bindir}
mv %{buildroot}/sbin/palo %{buildroot}/%{_bindir}
rmdir %{buildroot}/sbin

%files
%defattr(-, root, root, 0755)
%{_bindir}/palo
/usr/share/palo/iplboot

%doc
/usr/share/doc/palo/README
/usr/share/doc/palo/README.html
/usr/share/doc/palo/changelog.gz
/usr/share/doc/palo/palo.conf
/usr/share/man/man8/palo.8.gz

%changelog

