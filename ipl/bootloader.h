/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */
#undef __KERNEL__
#include "common.h"
#include <linux/types.h>
#include <stddef.h>

#define MAX_FD 20

#define __u64 unsigned long long

/* pdc_misc.c */
void die(const char *);
void firmware_init(int started_wide);
int pdc_default_width(int wide);
int pdc_bootdisk_2GB_limit(void);
int pdc_cons_duplex();
int pdc_cons_mux(int *is_mux);
int pdc_iodc_cin(char *buf, int size);
void pdc_iodc_cout(const char *c, int size);
int pdc_os_bits();
int pdc_iodc_bootin(__u64 devaddr, char *memaddr, unsigned size);
int pdc_read_conspath(unsigned char *memaddr);
int pdc_do_reset(void);
int pdc_coproc_cfg();
int pdc_model_sysmodel(char *name);

typedef void (*describe_t)(int fd, int *bufalign, int *blocksize);
typedef int (*read_t)(int fd, char *buf, unsigned nbytes, __u64 devaddr);

extern int Debug;
extern int disk_2gb_limit;

extern const char bld_info[];

/* pdc_bootio.c */
int pdc_bootdev_open(void);

/* byteio.c */
int byteio_open(int otherfd);

/* fileio.c */
int fileio_open(describe_t describef, read_t readf);
void fileio_close(unsigned int fd);
int seekread(int fd, char *buf, unsigned nbytes, __u64 devaddr);
void describe(int fd, int *bufalign, int *blocksize);

/* ext2.c */
int ext2_mount(long cons_dev, long p_offset, long quiet);
int ext2_open(const char *filename);
int ext2_filesize(int fd);
const char * ext2_readdir(int fd, int rewind, unsigned char *file_type);
#define EXT2_FT_DIR             2
#define EXT2_FT_SYMLINK         7
void ext2_close(int fd);

/* lib.c */
void blockprint(__u64 zero_offset, char *buf, int nbytes);
char *malloc_aligned(int nbytes, int align);
void *malloc(size_t nbytes);
void mark(void **ptr);
void release(void *ptr);
void malloc_init(char *first);
char *strpbrk(const char *cs, const char *ct);
size_t strspn(const char *s, const char *accept);
char *strtok(char *s, const char *ct);
int strncmp(const char *cs, const char *ct, size_t count);
void *memset(void *s, int c, size_t count);
int streq(const char *a, const char *b);
char *strcpy(char *dest, const char *src);
char *strcat(char *dest, const char *src);
void bzero(char *p, size_t len);
void *memcpy(void *d, const void *s, size_t len);
size_t strlen(const char *s);
size_t strnlen(const char *s, size_t count);
extern char *enter_text(char *txt, int maxchars);
extern char * strstr(const char * s1,const char * s2);
extern char * strchr(const char * s, int c);
extern char * strncpy(char * dest,const char *src,size_t count);
extern char * strrchr(const char * s, int c);

/* vsprintf.c */
int vsprintf(char *buf, const char *fmt, va_list args);
int sprintf(char *buf, const char *fmt, ...);

/* pdc_cons.c */
int getchar(void);
int puts(const char *s);
int putchar(int c);
int printf(const char *fmt, ...);

/* offset.c */
int offset_open(int otherfd, __u64 offset, __u64 length);

/* crt0.S */
typedef struct { unsigned save[32]; } jmp_buf;
int setjmp(jmp_buf env);
void longjmp(jmp_buf env, int val);
