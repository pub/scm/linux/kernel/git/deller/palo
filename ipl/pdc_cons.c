/* 
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) Hewlett-Packard (Paul Bame) paul_bame@hp.com
 */
#include <asm/pdc.h>
#include "bootloader.h"
#undef PAGE0
#define	PAGE0	((struct zeropage *)0x00000000)

/* don't wait for a character -- return 0 if none is currently available */
int
getchar(void)
{
    char buf;
    int count;
    
    count = pdc_iodc_cin(&buf, 1);
    if (count > 0)
    {
	return buf;
    }
    else
    {
	return 0;		/* got nothing, or error */
    }
}

static int
putstring(const char *s)
{
    int len_org = strlen(s);
    int len;
    char *p;

    /*
     * printing single chars is incredibly slow on C8000 machine.
     * optimize by printing a whole string but take care of new-lines.
     */
    do {
	len = strlen(s);
	if (len == 0)
		break;
	if (*s == '\n') {
		putchar(*s);
		s++;
		continue;
	}
	p = strchr(s, '\n');
	if (p)
		len = p - s;
        pdc_iodc_cout(s, len);
	s += len;
    } while (len);

    return len_org;
}

int
puts(const char *s)
{
    int len;

    len = putstring(s);

    /* puts() always adds a trailing '\n' */
    putchar('\n');

    return len;
}

int
putchar(int c)
{
    char ch = c;
    if (ch == '\n')
	pdc_iodc_cout("\r\n", 2);
    else
        pdc_iodc_cout(&ch, 1);
    return (unsigned char)ch;
}

int printf(const char *fmt, ...)
{
	char buf[4096];
	va_list args;
	int i;

	if (fmt[0] == 0)
		asm("\nprintf_test1: b,n .");

	va_start(args, fmt);
	vsprintf(buf, fmt, args);
	va_end(args);
	i = putstring(buf);
	return i;
}
