# Native PARISC/Linux Project Bootstrap Code
#
# Copyright (C) 2000 Hewlett Packard (Paul Bame, bame@puffin.external.hp.com)
# Copyright (C) 1999 Jason Eckhardt <jle@cygnus.com>
#                    Helge Deller <deller@gmx.de>
#                    Christopher Beard <cjbeard@thepuffingroup.com>
#                    Alex deVries <adevries@thepuffingroup.com>
#

# This is here temporarily for folks who still have the old linux makefile
ifneq ($(strip ${TOPDIR}),)
    PA=${TOPDIR}
endif

# here is one which should work for nearly everyone, unless they
# have a different name for their linux tree:
ifeq ($(strip ${PA}),)
    PA := $(shell pwd)/../linux
endif

export PA

# GNU make is needed, can be overridden by environment, e.g. for HP-UX
MAKE ?= make

# Choose at most one of RAMDISK or NFSROOT below
#
# Set RAMDISK to a ramdisk image to in order to load the ramdisk on the
# boot medium and make the default kernel command line to use it
#
# RAMDISK = ${PA}/ramdisk.bin

#
# Set NFSROOT to the IP address of your NFS root server to make
# a kernel command line to use NFS root.  This is incompatible with
# RAMDISK above.  Though you can have both a ramdisk and an NFS root,
# as far as palo is concerned anyway, this Makefile doesn't help you
# do that and I'm not sure the kernel's very good at it right now either.
#
NFSROOT = 10.10.10.2
# NFSROOT = 10.10.10.2:/tftpboot/nfsroot


ROOT=root=/dev/sda1

#
# In case you want to use ISA cards (or any edge triggered EISA cards) in your 
# EISA slots, just uncomment the following line and specify the corresponding 
# irq level(s)
#
# EISAIRQ = eisa_irq_edge=3,4,5,7,9,10,11,14,15


ifneq ($(strip ${RAMDISK}),)
    PALOSTUFF=-r ${RAMDISK}
    ROOT=root=/dev/ram initrd=0/ramdisk
endif

ifneq ($(strip ${NFSROOT}),)
    PALOSTUFF=
    ROOT=root=/dev/nfs nfsroot=${NFSROOT} ip=bootp
endif

CMDLINE = 0/vmlinux HOME=/ TERM=linux $(ROOT) $(EISAIRQ) $(CONSOLE)

HELP2MAN := $(shell command -v help2man 2> /dev/null)

all: makepalo README palo.conf

README:	README.html
	lynx -dump README.html > README

makepalo:
	cd palo && $(MAKE)

makeipl:
	cd ipl && $(MAKE)

lifimage:	all Makefile lifimage-only

lifimage-only: ${PA}/vmlinux iplboot
	./palo/palo -f /dev/null \
		-b iplboot \
		-k ${PA}/vmlinux \
		${PALOSTUFF} \
		-c "${CMDLINE}" \
		-s lifimage

iplboot:	makeipl makepalo ipl/iplelf
	./palo/mkbootable ipl/iplelf iplboot

clean:
	# We intentionally don't remove iplboot here so that the file
	# is available for cross-installation.
	rm -f lifimage palo.8.gz changelog.gz palo.tgz
	cd ipl && $(MAKE) clean
	cd palo && $(MAKE) clean

realclean:
	rm -f iplboot lifimage palo.8.gz changelog.gz README palo.tgz
	cd ipl && $(MAKE) clean
	cd palo && $(MAKE) clean

distclean:
	rm -f lifimage palo.8.gz changelog.gz README palo.tgz
	cd ipl && $(MAKE) distclean
	cd palo && $(MAKE) distclean

ifdef HELP2MAN
palo.8:		makepalo palo/usage.txt palo.help2man
	help2man --section=8 --no-info \
		--name="boot media management tool for PA-RISC/HPPA." \
		--include=palo.help2man ./palo/palo > palo.8
endif

palo.8.gz:	palo.8
	gzip -9 < palo.8 > palo.8.gz

changelog.gz:	debian/changelog
	gzip -9 < debian/changelog > changelog.gz

INSTALL	=	install
IFLAGS	=	-o root -g root
INSTALL	+=	${IFLAGS}
DESTDIR = ./root

SBINDIR ?= /sbin
CONFDIR ?= /etc
DATADIR ?= /usr/share
DOCDIR  ?= /usr/share/doc
MANDIR  ?= /usr/share/man

install: all README.html palo.conf palo/palo palo.8.gz changelog.gz
	${INSTALL} -d ${DESTDIR}${DOCDIR}/palo \
			${DESTDIR}${CONFDIR} ${DESTDIR}${SBINDIR} \
			${DESTDIR}${DATADIR}/palo \
			${DESTDIR}${MANDIR}/man8
	${INSTALL} -m644 palo.conf ${DESTDIR}${DOCDIR}/palo
	${INSTALL} palo/palo ${DESTDIR}$(SBINDIR)
	@# iplboot isn't natively executable -- don't set mode=x
	${INSTALL} -m644 iplboot ${DESTDIR}${DATADIR}/palo
	${INSTALL} -m644 README.html README changelog.gz \
			${DESTDIR}${DOCDIR}/palo
	${INSTALL} -m644 palo.8.gz ${DESTDIR}${MANDIR}/man8

uninstall: 
	${RM} -rf ${DESTDIR}${DOCDIR}/palo
	${RM} -rf ${DESTDIR}${SBINDIR}/palo
	${RM} -rf ${DESTDIR}${DATADIR}/palo
	${RM} -rf ${DESTDIR}${MANDIR}/man8/palo.8.gz

palo.tgz:
	tar cvfz palo.tgz *

# Build RPM source and binary package
palo.rpm rpm:
	rm -f palo.tgz && $(MAKE) palo.tgz && rpmbuild -ta palo.tgz

